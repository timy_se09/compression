package com.gateweb.compression.entity;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvoiceDetailsRepository  extends JpaRepository<InvoiceDetailsEntity, Long> {
  List<InvoiceDetailsEntity> findAllByYearMonthAndInvoiceNumberOrderBySequenceNumberDesc(String yearMonth, String invoiceNumber);
}
