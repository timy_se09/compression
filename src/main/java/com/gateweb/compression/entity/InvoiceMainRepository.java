package com.gateweb.compression.entity;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;

import javax.persistence.QueryHint;
import java.util.stream.Stream;


public interface InvoiceMainRepository extends JpaRepository<InvoiceMainEntity, Long> {
  @QueryHints(
      value = {
          @QueryHint(
              name = org.hibernate.jpa.QueryHints.HINT_FETCH_SIZE,
              value = "100000"
          ),
          @QueryHint(
              name = org.hibernate.jpa.QueryHints.HINT_READONLY,
              value = "true"
          ),
          @QueryHint(
              name = org.hibernate.jpa.QueryHints.HINT_CACHEABLE,
              value = "false"
          )
      }

  )
  Stream<InvoiceMainEntity> findAllByYearMonthAndSellerAndInvoiceDate(String yearMonth, String seller, String invoiceDate);
}
