package com.gateweb.compression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
public class CompressionApplication implements CommandLineRunner {

  private static Logger logger = LoggerFactory.getLogger(CompressionApplication.class);
  private CompressToAvro compressToAvro;

  public CompressionApplication(CompressToAvro compressToAvro) {
    this.compressToAvro = compressToAvro;
  }

  @Override
  @Transactional
  public void run(String... args) throws Exception {
//    final String root = "src/test/resources";
//    compressToAvro.toAvro(root, "raw", "avro");
//    compressToAvro.fromAvro(root, "avro", "result", ".csv");
  }

  public static void main(String[] args) {
    SpringApplication.run(CompressionApplication.class, args);
  }
}
