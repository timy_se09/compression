package com.gateweb.compression;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import com.gateweb.compression.model.Test;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import org.apache.avro.file.CodecFactory;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class CompressToAvro {

  private static Logger logger = LoggerFactory.getLogger(CompressToAvro.class);
  private ObjectMapper objectMapper;
  private XmlMapper xmlMapper;
  //  private AvroMapper avroMapper;

  public CompressToAvro(ObjectMapper objectMapper, XmlMapper xmlMapper) {
    this.objectMapper = objectMapper;
    this.xmlMapper = xmlMapper;
  }

  public void toAvro(String root, String sourceFolder, String targetFolder)
      throws IOException, CsvException {
    final Map<Path, Path> allFiles =
        findAllFiles(Paths.get(root), sourceFolder, targetFolder, ".avro");
    toAvro(allFiles);
  }

  public void toAvro(Map<Path, Path> allFiles) throws IOException, CsvException {
    for (Path sourceFile : allFiles.keySet()) {
      final Path targetFile = allFiles.get(sourceFile);
      compress(sourceFile, targetFile);
    }
  }

  public void fromAvro(Map<Path, Path> allFiles) throws IOException {
    for (Path sourceFile : allFiles.keySet()) {
      final Path targetFile = allFiles.get(sourceFile);
      uncompress(Test.class, sourceFile, targetFile);
    }
  }

  public void fromAvro(
      String root, String sourceFolder, String targetFolder, String targetExtension)
      throws IOException {
    final Map<Path, Path> allFiles =
        findAllFiles(Paths.get(root), sourceFolder, targetFolder, targetExtension);
    fromAvro(allFiles);
  }

  // 找出該目錄下所有檔案路徑
  private Map<Path, Path> findAllFiles(
      Path root, String sourceFolder, String targetFolder, String targetExtension)
      throws IOException {
    final Map<Path, Path> map = new LinkedHashMap<>();
    final Path source = root.resolve(sourceFolder);
    try (Stream<Path> stream = Files.walk(source).filter(Files::isRegularFile)) {
      stream.forEach(
          sourceFile -> {
            final String targetStr =
                StringUtils.substringBeforeLast(
                        sourceFile
                            .toString()
                            .replace(
                                File.separatorChar + sourceFolder,
                                File.separatorChar + targetFolder),
                        ".")
                    + targetExtension;
            final Path targetFile = Paths.get(targetStr);
            if (Files.notExists(targetFile)) {
              try {
                Files.createDirectories(targetFile.getParent());
              } catch (IOException e) {
                e.printStackTrace();
              }
            }
            map.put(sourceFile, targetFile);
          });
      return map;
    }
  }

  private void compress(Path sourceFile, Path targetFile) throws IOException, CsvException {
    final String extension = com.google.common.io.Files.getFileExtension(sourceFile.toString());
    List<Test> tests = new ArrayList<>();
    switch (extension) {
      case "json":
        break;
      case "xml":
        break;
      case "csv":
        // read csv
        try (CSVReader reader = new CSVReader(new FileReader(sourceFile.toFile()))) {
          // headers
          reader.readNext();
          tests =
              reader.readAll().stream()
                  .map(row -> Test.newBuilder().setHostName(row[0]).setIpAddress(row[1]).build())
                  .collect(Collectors.toList());
        }
        break;
    }
    if (tests.isEmpty()) {
      logger.error("no data");
      return;
    }

    // compress to avro
    final DatumWriter<Test> datumWriter = new SpecificDatumWriter<>(Test.class);
    try (DataFileWriter dataFileWriter = new DataFileWriter(datumWriter)) {
      dataFileWriter.setCodec(CodecFactory.snappyCodec());
      dataFileWriter.create(Test.SCHEMA$, targetFile.toFile());
      for (Test test : tests) {
        dataFileWriter.append(test);
      }
    }
    logger.info("[ToAvro OK] => {} to {}", sourceFile, targetFile);
  }

  private <T> void uncompress(Class<T> format, Path sourceFile, Path targetFile)
      throws IOException {
    // read avsc
    final List<T> raws = new ArrayList<>();

    final DatumReader<T> datumReader = new SpecificDatumReader<>(format);
    try (DataFileReader<T> dataFileReader = new DataFileReader(sourceFile.toFile(), datumReader)) {
      dataFileReader.previousSync();
      while (dataFileReader.hasNext()) {
        final T obj = dataFileReader.next();
        raws.add(obj);
        logger.info("{}", obj);
      }
    }
    final String extension = com.google.common.io.Files.getFileExtension(targetFile.toString());

    // write
    switch (extension) {
      case "json":
        // TODO
        break;
      case "xml":
        // TODO
        break;
      case "csv":
        //        final List<String[]> csvRaws = new ArrayList<>();
        final String[] fields = ReflectUtils.getPrivateFieldsWithoutStaticAndFinal(raws.get(0));
        logger.info("hearder: {}", StringUtils.join(fields, ","));

        // to String[]
        final List<String[]> csvRaws =
            raws.stream()
                .map(
                    raw ->
                        Arrays.stream(fields)
                            .map(field -> ReflectUtils.invokeGetter(raw, field))
                            .toArray(String[]::new))
                .collect(Collectors.toList());

        try (CSVWriter csvWriter = new CSVWriter(new FileWriter(targetFile.toFile()))) {
          csvWriter.writeNext(fields);
          csvWriter.writeAll(csvRaws);
        }
        break;
    }

    logger.info("[FromAvro OK] => {} to {}", sourceFile, targetFile);
  }

  private <T> void avroToXml(List<T> raws, Path targetFile) throws IOException {
    System.out.println("avro:");
    Map map = new HashMap();
    for (T t : raws) {
      Test test = (Test) t;
      String[] fields = ReflectUtils.getPrivateFieldsWithoutStaticAndFinal(test);
      // key name -> field name
      map.put(fields[0].toString(), test.getHostName());
      map.put(fields[1].toString(), test.getIpAddress());
      System.out.println(fields[0].toString() + " " + fields[1].toString());
    }

    // 序列化時加上文件頭信息
    xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
    xmlMapper.writerWithDefaultPrettyPrinter().writeValue(targetFile.toFile(), map);
  }

  abstract class IgnoreSchemaProperty {
    // You have to use the correct package for JsonIgnore,
    // fasterxml or codehaus
    @JsonIgnore
    abstract void getSchema();
  }
}
