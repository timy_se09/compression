package com.gateweb.compression;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

class CompressToAvroUnitTest {


  CompressToAvro compressToAvro = new CompressToAvro(new ObjectMapper());

  //  public CompressToAvroTest(CompressToAvro compressToAvro) {
  //    this.compressToAvro = compressToAvro;
  //  }

  //  @BeforeEach
  //  void setUp(CompressToAvro compressToAvro) {
  //      this.compressToAvro = compressToAvro;
  //  }

  //  @AfterEach
  //  void tearDown() {}

//  @Test
  void csvToAvro() {
    final String root = "src/test/resources";
    //    compressToAvro.toAvro(root, "raw", "avro");

  }

//  @Test
  void avroToCsv() throws IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
    final String root = "src/test/resources";
    compressToAvro.fromAvro(root, "avro", "result", ".csv");
  }

    @Test
    void avroToJson() throws IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        final String root = "src/test/resources";
        compressToAvro.fromAvro(root, "avro", "result", ".json");
    }


    private void checkCsvBom(Path path) throws IOException {
    // csv 才做
    if (path.getFileName().toString().indexOf(".csv") == -1) {
      return;
    }
    // 檢查 Bom
    final String bom = "\uFEFF";
    final List<String> list = Files.readAllLines(path, StandardCharsets.UTF_8);
    String firstRow = list.get(0);
    if (!StringUtils.startsWith(firstRow, bom)) {
      // 將 BOM 加到第一行
      firstRow = bom + list.get(0);
      list.remove(0);
      list.add(0, firstRow);
      Files.write(path, list, StandardCharsets.UTF_8);
      //            logger.debug("Add Bom to {}",path);
    }
  }
}
