package com.gateweb.compression;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.jupiter.api.Test;

// @SpringBootTest
class CompressionApplicationTests {

  @Test
  void gcle() {
    String aaa = "=\"123\"";
    final String s = aaa.replaceAll("[=\"]", "");
    System.out.println(s);
  }

  @Test
  void aaa() throws IOException {
    Path path = Paths.get("src/test/resources/raw").toAbsolutePath();
    Files.walk(path).filter(Files::isRegularFile).forEach(System.out::println);
  }

  @Test
  void fdsafd() throws IOException {
    Path path = Paths.get("src/test/resources/raw/csv/test.csv").toAbsolutePath();
    final String fileExtension = com.google.common.io.Files.getFileExtension(path.toString());
    System.out.println(fileExtension);
  }
}
