package com.gateweb.compression;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class CompressToAvroTest {

  @Autowired CompressToAvro compressToAvro;

  //  public CompressToAvroTest(CompressToAvro compressToAvro) {
  //    this.compressToAvro = compressToAvro;
  //  }

  //  @BeforeEach
  //  void setUp(CompressToAvro compressToAvro) {
  //      this.compressToAvro = compressToAvro;
  //  }

//  @AfterEach
//  void tearDown() {}

//  @Test
  void toAvro() {
    final String root = "src/test/resources";
    //    compressToAvro.toAvro(root, "raw", "avro");

  }

//  @Test
  void fromAvro() throws IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
    final String root = "src/test/resources";
    compressToAvro.fromAvro(root, "avro", "result", ".csv");
  }

}
